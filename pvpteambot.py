# bot.py
import os
import random
from discord.ext import commands
from dotenv import load_dotenv
import shelve

load_dotenv()
TOKEN = os.getenv('DISCORD_TOKEN')

bot = commands.Bot(command_prefix='!')

shelf = shelve.open("./teams", "n", writeback=True);

@bot.command(name='match', help='Creates a match.')
async def roll(ctx, user1: str, user2: str):
    n = len(shelf) + 1
    shelf[f'{n:04}'] = {'match_code': f'{n:04}',
                     'user1': user1,
                     'user2': user2,
                     'teams': {
                         user1:{
                             'team': ''
                         },
                         user2: {
                             'team': ''
                         }
                     }
                     }
    print(shelf[f'{n:04}'])
    await ctx.send(f'Set! {user1} vs {user2}. MATCH CODE: {n:04}')
    
@bot.command(name='team', help='Set team.')
async def roll(ctx, match_code: str,  user: str, slot1: str, slot2: str, slot3: str, slot4: str, slot5: str, slot6: str):
    if match_code in shelf:
        if shelf[match_code]['user1'] == user or shelf[match_code]['user2'] == user:
            if shelf[match_code]['teams'][user]['team'] == '':
                shelf[match_code]['teams'][user]['team'] = f'{slot1}, {slot2}, {slot3}, {slot4}, {slot5}, {slot6}'
                print(shelf[match_code])
                await ctx.send(f'Set! MATCH CODE: {match_code}. User: {user}. TEAM: {slot1}, {slot2}, {slot3}, {slot4}, {slot5}, {slot6}')
            else:
                await ctx.send(f'Team has already been set.')
        else:
            await ctx.send(f'User {user} does not exist in match.')
    else:
        await ctx.send(f'Match {match_code} does not exist.')

@bot.command(name='opponent', help='Get opponent team.')
async def roll(ctx, match_code: str):
    if match_code in shelf:
        users = [shelf[match_code]['user1'], shelf[match_code]['user2']]
        if (shelf[match_code]['teams'][users[0]]['team'] != '') and (shelf[match_code]['teams'][users[1]]['team'] != ''):
            await ctx.send(f"{match_code} Your teams are: \n {users[0]}: {shelf[match_code]['teams'][users[0]]['team']}  \n {users[1]}: {shelf[match_code]['teams'][users[1]]['team']}")
        else:
            await ctx.send(f'Teams not ready!')

bot.run(TOKEN)
